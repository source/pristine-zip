# Copyright (C) 2020 The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import os
import subprocess
import tempfile

from . import common
from . import parameters
from . import reference


def genzip(
    executables: common.Executables,
    checkout_dir: str,
    delta_path: str,
    zipball_path: str,
):
    with tempfile.TemporaryDirectory(prefix="pristine-zip-genzip") as work_dir:
        reference_zipball_path = os.path.join(work_dir, "reference.zip")

        encoding_software = _extract_delta(work_dir, delta_path)

        # generate reference zipball
        reference.compress(
            executables, encoding_software, checkout_dir, reference_zipball_path
        )

        _apply_delta(reference_zipball_path, zipball_path, work_dir)


def _extract_delta(work_dir: str, delta_path: str) -> parameters.EncodingSoftware:
    proc = subprocess.run(["tar", "--extract", "-f", delta_path,], cwd=work_dir)
    proc.check_returncode()

    with open(os.path.join(work_dir, "type"), "rb") as fd:
        type_ = fd.read().decode().strip()
        assert type_ == "zip", (
            f"Unknown zipball type {type}. Are you"
            f"extracting a delta from pristine-tar instead of pristine-zip?"
        )

    with open(os.path.join(work_dir, "encoding_software"), "rb") as fd:
        encoding_software_str = fd.read().decode().strip()

    try:
        encoding_software = parameters.EncodingSoftware(encoding_software_str)
    except ValueError:
        raise common.PristineZipException(
            f"Delta file refers to unknown encoding software: "
            f"{encoding_software_str}. It may have been created by a newer version "
            f"of pristine-zip than the one you are currently running."
        )

    return encoding_software


def _apply_delta(
    reference_zipball_path: str, zipball_path: str, work_dir: str,
):
    with open(os.path.join(work_dir, "reference_md5sum"), "rb") as fd:
        expected_md5sum = fd.read().decode().strip()

    proc = subprocess.run(["md5sum", reference_zipball_path], capture_output=True)
    proc.check_returncode()
    actual_md5sum = proc.stdout.decode().split(" ", 1)[0].strip()

    if actual_md5sum != expected_md5sum:
        print(
            f"md5sum mismatch between reference zipballs "
            f"(expected '{expected_md5sum}', got '{actual_md5sum}').\n"
            f"This is a bug, please report it along with the original zipball "
            f"and the version number of pristine-zip."
        )
        exit(1)

    xdelta3_path = os.path.join(work_dir, "delta")
    assert os.path.isfile(xdelta3_path), "Missing 'delta' file in delta archive."

    _apply_xdelta3(reference_zipball_path, zipball_path, xdelta3_path)


def _apply_xdelta3(reference_zipball_path: str, zipball_path: str, xdelta3_path: str):
    try:
        os.remove(zipball_path)
    except FileNotFoundError:
        pass
    proc = subprocess.run(
        ["xdelta3", "-d", "-s", reference_zipball_path, xdelta3_path, zipball_path]
    )
    proc.check_returncode()
