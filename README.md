pristine-zip
============

Like pristine-tar, but of ZIP files: regenerate a pristine upstream zipball
using only a small binary delta file and a revision control checkout of the
upstream branch.

# Development status

Currently one works for InfoZIP and 7zip.

pristine-zip is currently of proof-of-concept quality. It was only tested on
a few ZIP files; and provides no stability guarantee if the deltas are not
read with the same pristine-zip/InfoZIP/7zip versions as they were written.
